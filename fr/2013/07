#use wml::debian::projectnews::header PUBDATE="2013-04-01" SUMMARY="Brèves de l'équipe de publication, brèves de l'équipe des administrateurs système Debian"
#use wml::debian::translation-check translation="1.1" maintainer="Cédric Boutillier"

# $Id: index.wml 1641 2011-02-11 02:13:03Z taffit-guest $
# $Rev: 1641 $
# Status: [reviewing]

## substitute XXX with the number (expressed in letter) of the issue.
## please note that the var issue is not automagically localized, so
## translators need to put it directly in their language!
## example: <intro issue="fourth" />

<intro issue="septième" />
<toc-display/>


<toc-add-entry name="rtbits">Brèves de l'équipe de publication</toc-add-entry>
<p>
Julien Cristau a envoyé des
<a href="http://lists.debian.org/debian-devel-announce/2013/03/msg00009.html">nouvelles
des activités de l'équipe de publication</a> annonçant le début de la
phase finale du gel.
Cela signifie qu'à partir de maintenant, la politique sur les
changements qui pourront être envoyés sera encore plus stricte (seules
les corrections de bogues critiques pour la publication seront
acceptées) et que l'équipe de publication commencera à appliquer une
étiquette spéciale 
(<a
href="http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=release.debian.org@packages.debian.org;tag=wheezy-can-defer;tag=wheezy-will-remove;tag=wheezy-is-blocker;ordering=wheezy-sort"><tt>wheezy-will-remove</tt></a>)
pour marquer les paquets qui seront retirés de la future version stable
si leurs bogues critiques pour la publication ne sont pas
résolus.
Julien a rappelé aussi que de l'aide est nécessaire pour finaliser les
notes de publication : tous les utilisateurs et contributeurs sont
invités à vérifier le 
<a href="http://www.debian.org/releases/wheezy/releasenotes">travail en
cours</a> et à aider à résoudre les <a
href="http://bugs.debian.org/cgi-bin/pkgreport.cgi?pkg=release-notes;tag=wheezy">problèmes restants</a>.
</p>


<toc-add-entry name="dsabits">Brèves de l'équipe des administrateurs système Debian</toc-add-entry>

<p>
Martin Zobel-Helas a envoyé quelques
<a href="http://lists.debian.org/debian-devel-announce/2013/03/msg00010.html">brèves
de l'équipe des administrateurs système Debian (DSA)</a> dans lesquelles
il rend compte des activités récentes de l'équipe.
Durant ces derniers mois, l'équipe s'est concentrée sur la
virtualisation de plusieurs machines physiques en créant quelques
grappes ganeti pour gérer les différentes machines virtuelles.
De plus, l'équipe a commencé un travail de réécriture de son code de
gestion des comptes en utilisant l'environnement de développement Django.
<br />
Le message se termine avec les remerciements de l'équipe à tous les
donateurs et les partenaires hébergeurs (et plus particulièrement <a
href="http://www.eaton.com">Eaton</a> et <a
href="http://www.brainfood.com">Brainfood</a>) ainsi que les
responsables du paquet ganeti, Iustin Pop et Guido Trotter.
</p>


<toc-add-entry name="interviews">Entretiens</toc-add-entry>

<p>
Le <a href="http://bits.debian.org">blog officiel de Debian</a>, lancé
récemment, contient des entretiens avec les trois candidats à l'élection 2013
du chef du projet Debian :
<a href="http://bits.debian.org/2013/03/dpl-interview-2013-moray.html">Moray Allan</a>, 
<a href="http://bits.debian.org/2013/03/dpl-interview-2013-algernon.html">Gergely Nagy</a>
et <a href="http://bits.debian.org/2013/03/dpl-interview-2013-lucas.html">Lucas Nussbaum</a>.
</p>


<toc-add-entry name="other">Autres nouvelles</toc-add-entry>

<p>
Stefano Zacchiroli a publié son
<a href="http://upsilon.cc/~zack/blog/posts/2013/03/bits_from_the_DPL_for_February_2013_and_a_half/">compte-rendu
mensuel d'activités en tant que chef du projet Debian</a>.
</p>

<p>
Jan Wagner a publié un
<a href="http://blog.waja.info/2013/03/20/chemnitzer-linux-tage-2013/">rapport
détaillé</a> sur la présence de Debian aux journées Linux 2013 de
Chemnitz.
</p>

<p>
Masayuki Hatta a écrit quelques conseils sur
<a href="http://www.mhatta.org/blog/2013/03/21/recipe-for-debian-haskell-packaging/">la
manière de créer des paquets pour les logiciels en Haskell</a>.
</p>

<p>
Rhonda a annoncé l'\
<a href="http://rhonda.deb.at/blog/debian/backports-integrated-into-the-main-archive.html">intégration
des services de rétroportage dans l'archive principale</a>.
</p>



<toc-add-entry name="newcontributors">Nouveaux développeurs et mainteneurs</toc-add-entry>

<p>
<a href="https://nm.debian.org/public/nmlist#done">Neuf candidats</a>
	ont été acceptés comme développeurs Debian et <a
href="http://udd.debian.org/cgi-bin/new-maintainers.cgi">deux personnes</a>
	ont commencé à maintenir des paquets depuis la dernière édition des
	« Nouvelles du projet Debian ». Bienvenue à 
#DDs
Dmitry Smirnov,
Jérémy Lal,
Matthias Klumpp,
Stephen Kitt,
Ivo De Decker,
Mike Gabriel,
Manuel A. Fernandez Montecelo,
Andrew Shadura,
Florian Schlichting,
#maintainers
Vamsee Kanakala
et
Sebastian Geiger

dans le projet !</p>


<toc-add-entry name="rcstats">Statistiques des bogues critiques pour la prochaine version</toc-add-entry>

<rcstatslink release="Wheezy"
	url="http://richardhartmann.de/blog/posts/2013/03/29-Debian_Release_Critical_Bug_report_for_Week_13/"
	testing="50"
	tobefixed="24" />


<toc-add-entry name="dsa">Annonces de sécurité Debian importantes</toc-add-entry>

	<p>L'équipe de sécurité de Debian a publié récemment des annonces
	concernant (entre autres) les paquets
<a href="$(HOME)/security/2013/dsa-2651">smokeping</a>,
<a href="$(HOME)/security/2013/dsa-2652">libxml2</a>,
<a href="$(HOME)/security/2013/dsa-2653">icinga</a>,
<a href="$(HOME)/security/2013/dsa-2655">rails</a> et
<a href="$(HOME)/security/2013/dsa-2656">bind9</a>.
	Veuillez les lire attentivement et prendre les mesures appropriées.</p>


<p>
        L'équipe en charge de la publication stable a publié une annonce concernant le paquet
<a href="http://lists.debian.org/debian-stable-announce/2013/03/msg00000.html">clamav</a>.
        Veuillez la lire attentivement et prendre les mesures appropriées.
</p>

<p>
Veuillez noter que ce sont uniquement les annonces les plus importantes 
des dernières semaines. Si vous désirez être au courant des dernières
annonces de l'équipe de sécurité de Debian, inscrivez-vous à la <a
href="http://lists.debian.org/debian-security-announce/">liste de 
diffusion correspondante</a> (ainsi qu'à la <a
href="http://lists.debian.org/debian-backports-announce/">liste de 
diffusion spécifique aux rétroportages</a> et celle des <a
href="http://lists.debian.org/debian-stable-announce/">mises
à jour de stable</a>).
</p>


<toc-add-entry name="nnwp">Nouveaux paquets dignes d'intérêt</toc-add-entry>

	<p>
129 paquets ont été ajoutés récemment à l'archive <q>unstable</q> de Debian.

	<a href="http://packages.debian.org/unstable/main/newpkg">\
	Parmi bien d'autres</a>, en voici une courte sélection :
	</p>

<ul>
<li><a href="http://packages.debian.org/unstable/main/gnome-shell-extension-weather">gnome-shell-extension-weather — extension de météo pour GNOME Shell</a> ;</li>
<li><a href="http://packages.debian.org/unstable/main/i18nspector">i18nspector — outil de vérification pour les fichiers gettext POT, PO et MO</a> ;</li>
<li><a href="http://packages.debian.org/unstable/main/license-reconcile">license-reconcile — outil pour réconcilier le code source et le fichier de copyright</a> ;</li>
<li><a href="http://packages.debian.org/unstable/main/silan">silan — outil en ligne de commande pour détecter les silences dans les fichiers audio</a> ;</li>
<li><a href="http://packages.debian.org/unstable/main/ultracopier">ultracopier — système avancé de copie de fichiers avec interface graphique</a>.</li>
</ul>


<toc-add-entry name="wnpp">Paquets qui ont besoin de travail</toc-add-entry>

## link= link to the mail report from wnpp@debian.org to debian-devel ML
## orphaned= number of packages orphaned according to $link
## rfa= number of packages up for adoption according to $link

<wnpp link="http://lists.debian.org/debian-devel/2013/03/msg00516.html"
	orphaned="491"
	rfa="145" />


<toc-add-entry name="continuedpn">Vous voulez continuer à lire les Nouvelles du projet Debian ?</toc-add-entry>

<continue-dpn />

#use wml::debian::projectnews::footer editor="Moray Allan, Cédric Boutillier, Francesca Ciceri, Justin B Rye" translator="Cédric Boutillier, l\'équipe francophone de traduction"

